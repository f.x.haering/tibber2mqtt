FROM mcr.microsoft.com/dotnet/runtime:6.0
COPY build /app
ENTRYPOINT ["dotnet", "app/Tibber2MQTT.dll"]
