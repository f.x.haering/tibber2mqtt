for /F "tokens=*" %%A in (%userprofile%/.tibber2mqtt/config) do (
  ssh itools.de -l fh "docker ps -aq -f \"name=tibber2mqtt\" | (xargs docker stop || true) | (xargs docker rm || true) && (docker rmi franz1960/tibber2mqtt || true) && docker run -d --restart unless-stopped --name tibber2mqtt franz1960/tibber2mqtt:latest %%A"
)

pause
