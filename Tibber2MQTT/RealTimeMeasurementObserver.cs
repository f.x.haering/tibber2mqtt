using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Json.Nodes;
using System.Threading.Tasks;
using Tibber.Sdk;

namespace Tibber2MQTT
{
  public class RealTimeMeasurementObserver : IObserver<RealTimeMeasurement>
  {
    public RealTimeMeasurementObserver(string sMqttTopic, Action hasCompleted) {
      this.MqttTopic = sMqttTopic;
      this.HasCompleted = hasCompleted;
    }
    private readonly string MqttTopic;
    private readonly Action HasCompleted;
    private DateTimeOffset? RecentDataLogTimestamp = null;
    public int DataRecvCount { get; private set; } = 0;
    public void OnCompleted() {
      Console.WriteLine("Real time measurement stream has been terminated. ");
      this?.HasCompleted();
    }
    public void OnError(Exception error) => Console.WriteLine($"An error occured: {error}");
    public void OnNext(RealTimeMeasurement value) {
      try {
        this.DataRecvCount++;
        if (!this.recentDateTime.HasValue || (value.Timestamp.DateTime - this.recentDateTime.Value).TotalSeconds >= 20) {
          var lProps = new List<string[]>();
          //lProps.Add(new string[] { "ts", "'" + ConvInvar.ToIsoDateTime(value.Timestamp.DateTime) + "'" });
          lProps.Add(new string[] { "Timestamp", "'" + ConvInvar.ToIsoDateTimeOffset(value.Timestamp) + "'" });
          lProps.Add(new string[] { "LastMeterConsumption", ConvInvar.ToDecimalString(value.LastMeterConsumption.Value, 4) });
          lProps.Add(new string[] { "LastMeterProduction", ConvInvar.ToDecimalString(value.LastMeterProduction.Value, 4) });
          if (!string.IsNullOrEmpty(value.Currency)) {
            lProps.Add(new string[] { "Currency", "'" + value.Currency + "'" });
          }
          if (value.AccumulatedCost.HasValue) {
            lProps.Add(new string[] { "AccumulatedCost", ConvInvar.ToDecimalString(value.AccumulatedCost.Value, 2) });
          }
          var sbJsonValue = new StringBuilder();
          sbJsonValue.Append("{");
          for (int i = 0; i < lProps.Count; i++) {
            if (i > 0) {
              sbJsonValue.Append(",");
            }
            sbJsonValue.Append("'");
            sbJsonValue.Append(lProps[i][0]);
            sbJsonValue.Append("':");
            sbJsonValue.Append(lProps[i][1]);
          }
          sbJsonValue.Append("}");
          var sJsonValue = sbJsonValue.ToString();
          Debug.WriteLine(sJsonValue);
          //MqttXfer.Mosquitto_pub(this.MqttTopic, "{'ts':'" + ConvInvar.ToIsoDateTime(value.Timestamp.DateTime) + "','LastMeterConsumption':" + ConvInvar.ToDecimalString(value.LastMeterConsumption.Value, 4) + ",'LastMeterProduction':" + ConvInvar.ToDecimalString(value.LastMeterProduction.Value, 4) + "}");
          MqttXfer.Mosquitto_pub(this.MqttTopic, sJsonValue);
          this.recentDateTime = value.Timestamp.DateTime;
          // Write data to log every hour.
          if (!this.RecentDataLogTimestamp.HasValue || (DateTimeOffset.Now - this.RecentDataLogTimestamp.Value).TotalHours >= 1) {
            Console.WriteLine($"Data transferred: {sJsonValue}");
            this.RecentDataLogTimestamp = DateTimeOffset.Now;
          }
        }
      } catch (Exception ex) {
        Console.WriteLine($"Exception processing new data: {ex.ToString()}");
      }
    }
    private DateTime? recentDateTime = null;
  }
}
