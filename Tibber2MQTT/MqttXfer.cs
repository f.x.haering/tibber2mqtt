using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tibber2MQTT
{
  public class MqttXfer
  {
    public static async Task Mosquitto_pub(string sTopic, string sMessage) {
      try {
        var client = new System.Net.Http.HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

        var requestContent = new System.Net.Http.StringContent("{\"topic\":\"" + sTopic + "\",\"message\":\"" + sMessage + "\"}");
        requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
        var httpResponse = await client.PostAsync("https://api.itools.de/api/MosquittoPub", requestContent);
        //await client.PostAsync("https://localhost:7047/api/Mosquitto_pub", requestContent);

        //var requestContent = new System.Net.Http.StringContent("{\"Cmd\":\"mosquitto_pub\",\"topic\":\"" + sTopic + "\",\"message\":\"" + sMessage + "\"}");
        //requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
        //var httpResponse = await client.PostAsync("https://itools.de/rfapi/rfapi.php", requestContent);

        var memStream = new System.IO.MemoryStream();
        await httpResponse.Content.CopyToAsync(memStream);
        string sResponse = Encoding.UTF8.GetString(memStream.GetBuffer());
        await Task.Delay(200);
      } catch (Exception ex) {
        System.Diagnostics.Debug.WriteLine("Exception in Mosquitto_pub: " + ex.ToString());
      }
    }

  }
}
