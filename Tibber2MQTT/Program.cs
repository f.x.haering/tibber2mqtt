using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json.Nodes;
using Newtonsoft.Json.Linq;
using Tibber.Sdk;

namespace Tibber2MQTT
{
  internal class Program
  {
    private static string MqttTopic;
    private static string TibberAccessToken;
    static async Task<int> Main(string[] args) {
      if (args.Length < 2) {
        Console.WriteLine($"Starting Tibber2MQTT.Main({args.Aggregate((a, b) => a + "," + b)})...");
        Console.WriteLine("Not enough arguments on command line.");
        return 2;
      }
      MqttTopic = args[0];
      TibberAccessToken = args[1];
      var accessToken = TibberAccessToken;
      var userAgent = new ProductInfoHeaderValue("Tibber2MQTT", System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString());
      Console.WriteLine("Starting Tibber2MQTT.");
      var myTibberData = await GetDataFromTibber(accessToken, userAgent);
      Price price = myTibberData.Data.Viewer.Home.CurrentSubscription.PriceInfo.Current;
      Console.WriteLine($"Login: {myTibberData.Data.Viewer.Login}");
      Console.WriteLine($"Transferring data from {myTibberData.Data.Viewer.Home.Address.City}, {myTibberData.Data.Viewer.Home.Address.Address1} owned by {myTibberData.Data.Viewer.Home.Owner.Name} to MQTT topic '{MqttTopic}'.");
      Console.WriteLine($"RealTimeConsumptionEnabled: {myTibberData.Data.Viewer.Home.Features.RealTimeConsumptionEnabled}");
      Console.WriteLine($"Current price: {ConvInvar.ToDecimalString(price?.Total, 4)} {price?.Currency} starts at {price?.StartsAt}");
      if (price != null) {
        // Send price info to MQTT.
        var lProps = new List<string[]>();
        lProps.Add(new string[] { "CurrentTotal", ConvInvar.ToDecimalString(price.Total, 4) });
        lProps.Add(new string[] { "Currency", "'" + price.Currency + "'" });
        lProps.Add(new string[] { "StartsAt", "'" + price.StartsAt + "'" });
        var sbJsonValue = new StringBuilder();
        sbJsonValue.Append("{");
        for (int i = 0; i < lProps.Count; i++) {
          if (i > 0) {
            sbJsonValue.Append(",");
          }
          sbJsonValue.Append("'");
          sbJsonValue.Append(lProps[i][0]);
          sbJsonValue.Append("':");
          sbJsonValue.Append(lProps[i][1]);
        }
        sbJsonValue.Append("}");
        var sJsonValue = sbJsonValue.ToString();
        Console.WriteLine($"Price info to MQTT: {sJsonValue}");
        await MqttXfer.Mosquitto_pub(MqttTopic + "/PriceInfo", sJsonValue);
      }
      //
      try {
        // Initialize
        // Please set user agent so we can track different client implementations
        var client = new TibberApiClient(accessToken, userAgent);
        Guid homeId = myTibberData.Data.Viewer.Home.Id.Value;
        var cancellationTokenSource = new CancellationTokenSource();
        var listener = await client.StartRealTimeMeasurementListener(homeId, cancellationTokenSource.Token);
        var rtmObserver = new RealTimeMeasurementObserver(MqttTopic + "/RealTime", () => cancellationTokenSource.Cancel());
        listener.Subscribe(rtmObserver);
        // End program after certain time.
        await Task.Run(async ()=>{
          await Task.Delay(600000);
        });
        Console.WriteLine("Stop listening");
        // Stop listening
        await client.StopRealTimeMeasurementListener(homeId);
      } catch (Exception ex) {
        Console.WriteLine($"Exception in main loop: {ex}");
      }
      Console.WriteLine("Exiting");
      await Task.Delay(35000);
      return 0;
    }
    public static async Task<TibberApiQueryResponse> GetDataFromTibber(string accessToken, ProductInfoHeaderValue userAgent) {
      var client = new TibberApiClient(accessToken, userAgent);

      var basicData = await client.GetBasicData();
      var homeId = basicData.Data.Viewer.Homes.First().Id.Value;
      var consumption = await client.GetHomeConsumption(homeId, EnergyResolution.Monthly);

      var customQueryBuilder =
          new TibberQueryBuilder()
              .WithAllScalarFields()
              .WithViewer(
                  new ViewerQueryBuilder()
                      .WithAllScalarFields()
                      .WithAccountType()
                      .WithHome(
                          new HomeQueryBuilder()
                              .WithAllScalarFields()
                              .WithAddress(new AddressQueryBuilder().WithAllFields())
                              .WithCurrentSubscription(
                                  new SubscriptionQueryBuilder()
                                      .WithAllScalarFields()
                                      .WithSubscriber(new LegalEntityQueryBuilder().WithAllFields())
                                      .WithPriceInfo(new PriceInfoQueryBuilder().WithCurrent(new PriceQueryBuilder().WithAllFields()))
                              )
                              .WithOwner(new LegalEntityQueryBuilder().WithAllFields())
                              .WithFeatures(new HomeFeaturesQueryBuilder().WithAllFields())
                              .WithMeteringPointData(new MeteringPointDataQueryBuilder().WithAllFields()),
                          homeId
                      )
              );

      var customQuery = customQueryBuilder.Build(); // produces plain GraphQL query text
      var result = await client.Query(customQuery);
      return result;
    }
  }
}
